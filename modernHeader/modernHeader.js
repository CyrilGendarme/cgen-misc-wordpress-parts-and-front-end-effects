jQuery(function($){
    $(document).ready(function(){  

        
        var lang = document.documentElement.lang.substring(0, 2);
        var parentElements = document.querySelectorAll("#websiteLinks, #logo, #footerLinks, #legalLinksFooter");
        for (var j = 0; j < parentElements.length; j++) {
            var childElements = parentElements[j].getElementsByTagName('a');
            for (var i = 0; i < childElements.length; i++) {
            var href = childElements[i].getAttribute('href');
            childElements[i].setAttribute('href', '/' + lang + href);
            }
        }

            

        // All variables needed in functions  
        logo = $('.logo');
        var menuSection = $('.fullwidth-menu');
        var hamburgerIcon = $('.fullwidth-open');
        var menuLink = $('.fullwidth-menu a');
        var subMenu = $('.main-menu-item ul');
        var toggleIcon = $('.toggle-sub-menu');
        var languageSwitch = $('.cgen-languageSwitch');
        var parentSubMenu = $('.parent-sub-menu');
        var hamburgerToggleIcon = $('.cgen-hamburgerMenu-toggle');
        var cgenMenuIcons  = $('.cgen-menuIcons, .cgen-languageSwitch');

            

        // Add the aria-label attribute -> for accessibility/SEO purpose
        const cart = $('.et_pb_menu__cart-button');
        cart.attr('aria-label', 'Shopping Cart');
            
        // Open fullwidth menu & animate items  
        hamburgerIcon.click(function(){
            hamburgerToggleIcon.toggleClass('blackBG');
            $(this).toggleClass('open');
            logo.toggleClass('displayNone');
            menuSection.toggleClass('fullwidth-menu-open');
            if (menuSection.hasClass("fullwidth-menu-open")) {
            } else { 
                toggleIcon.text($(this).text() == '+' ? '-' : '+');  
            }
            cgenMenuIcons.toggleClass('cgen-menuIcons--menuOpen');
            
        });
            
        // Change icon when toggling the submenu  
        toggleIcon.click(function(){  
            var subMenu = $(this).parent().find("ul");
            subMenu.slideToggle(); 
            $(this).text($(this).text() == '-' ? '+' : '-'); 
        });  
        languageSwitch.click(function(){  
            var subMenu = $(this).parent().find("ul");
            subMenu.slideToggle(); 
        });  
        parentSubMenu.click(function(){  
            var subMenu = $(this).parent().find("ul");
            subMenu.slideToggle();
        });  
    });
});