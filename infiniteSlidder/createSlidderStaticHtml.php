<?php

function checkLang()
{
    $currentUrl = $_SERVER['REQUEST_URI'];

    $urlParts = explode('/', trim($currentUrl, '/'));

    if (count($urlParts) == 0) {
        return 'fr';
    }

    $secondElement = isset($urlParts[0]) ? $urlParts[0] : '';

    if ($secondElement === 'en' || $secondElement === 'nl') {
        return $secondElement;
    }

    return 'fr';
}





function cgen_load_html_file($file) {
	$lang = checkLang();
	$plugin_dir = dirname(plugin_dir_path(__FILE__));
 	$file_path = $plugin_dir . '/templates/' . $lang . '/' . $file . '.html';
	
	
    ob_start();
    include_once($file_path);
    $content = ob_get_clean();
	
    return $content;
}



function cgen_write_to_html_file($file, $content, $lang) {

	$plugin_dir = dirname(plugin_dir_path(__FILE__));
	
 	$file_path = $plugin_dir . '/templates/' . $lang . '/' . $file . '.html';
	
	// Check if the directory exists, if not, create it recursively
    $dir_path = dirname($file_path);
    if (!is_dir($dir_path)) {
        mkdir($dir_path, 0755, true);
    }
	
    if (is_writable($file_path) || touch($file_path)) {
        file_put_contents($file_path, $content);
        return true;
    } else {
        return false;
    }
}




function createSliderContainer($link, $name, $img)
{
    $html = '';
    $html .= '<div class="cgen-glitchContainer">';
    $html .= '<a href="' . $link . '" class="cgen-container">';
    $html .= '<div class="cgen-image-container">';
    $html .= '<div class="cgen-caroussel--img">';
    $html .= '<img src="' . $img . '" alt="' . $name . '">';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '<div class="cgen-caroussel--txt">';
    $html .= '<p>' . $name . '</p>';
    $html .= '</div>';
    $html .= '</a>';
    $html .= '</div>';
    return $html;
}

function cgenInfiniteSlidder__contentCreate($lang){

    if ($lang == "fr") {
        $list = array("tag1", "Cyril Gendarme", "Tourbicouille", "Test 2");
        $list2 = array('t', 'c', 'p', 'a');
    } else if ($lang == "en") {
        $list = array("tag1", "Cyril Gendarme", "Tourbicouille");
        $list2 = array('t', 'c', 'p');
    } else if ($lang == "nl") {
        $list = array("tag1", "Cyril Gendarme");
        $list2 = array('t', 'c');
    } else {
        return "<div> ERREUR : " . $lang . "</div>";
    }

    // MIN LIST SIZE = 6
    while (count($list) < 6) {
        $list = array_merge($list, $list);
        $list2 = array_merge($list2, $list2);
    }

    $output = '<div class="image-container">';
    $output .= '<div class="image-wrapper">';


    $sites = get_sites();
    $temp="";
    foreach ($sites as $site) {
		if ("/".$lang."/" ===  $site->path){
			 switch_to_blog($site->blog_id);
		}
    }


	
    foreach ($list as $index => $name) {
        $type = $list2[$index];

        switch ($type) {
            case 't': // TAG
                $tag = get_term_by('name', $name, 'product_tag');
                if ($tag) {
                    $tag_id = $tag->term_id;
                    $tag_url = get_tag_link($tag_id);
                    $thumbnail= wp_get_attachment_image_src(get_field('image', 'product_tag_' . $tag_id), 'full')[0];
                    $output .= createSliderContainer($tag_url, $name,  $thumbnail);
                } else {
                    $output .= "<div>BUG 1</div>";
                }
                break;
            case 'c': // CATEGORY
                $category = get_term_by('name', $name, 'product_cat');
                if ($category) {
                    $category_id = $category->term_id;
                    $category_url = get_term_link($category_id, 'product_cat');
                    $thumbnail = wp_get_attachment_image_src(get_field('image', 'product_cat_' . $category_id), 'full')[0];
                    $output .= createSliderContainer($category_url, $name, $thumbnail);
                } else {
                    $output .= "<div>BUG 2</div>";
                }
                break;
            case 'p': // PRODUCT
                $product = get_page_by_title($name, OBJECT, 'product');
                if ($product) {
                    $product_id = $product->ID;
                    $product_url = get_permalink($product_id);
                    $thumbnail = get_the_post_thumbnail_url($product_id, 'full');
                    $output .= createSliderContainer($product_url, $name, $thumbnail);
                } else {
                    $output .= "<div>BUG 3</div>";
                }
                break;
            case 'a': // ARTICLE POST
                $post = get_page_by_title($name, OBJECT, 'post');
                if ($post) {
                    $post_id = $post->ID;
                    $permalink = get_permalink($post_id);
                    $thumbnail = get_the_post_thumbnail_url($post_id, 'full');
                    $output .= createSliderContainer($permalink, $name, $thumbnail);
                } else {
                    $output .= "<div>BUG 4</div>";
                }
                break;
            default:
                $output .= "<div> BUG line 394 createHtmlParts.php </div>";
                break;
        }
    }

	restore_current_blog();
	
    $output .= '</div></div>';
    return $output;
}



function cgenInfiniteSlidder__func()
{
    $file = 'cgen-infiniteSlidder';
    return cgen_load_html_file($file);

}
add_shortcode('cgen-infiniteSlidder', 'cgenInfiniteSlidder__func');



function createAllHtmlDynamicParts(){
    cgen_write_to_html_file('cgen-infiniteSlidder', cgenInfiniteSlidder__contentCreate('fr'), 'fr' );
    cgen_write_to_html_file('cgen-infiniteSlidder', cgenInfiniteSlidder__contentCreate('en'), 'en' );
    cgen_write_to_html_file('cgen-infiniteSlidder', cgenInfiniteSlidder__contentCreate('nl'), 'nl' );
}


?>