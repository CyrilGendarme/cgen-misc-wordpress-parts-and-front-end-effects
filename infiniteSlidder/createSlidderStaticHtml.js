const imageWrapper = document.querySelector('.image-wrapper');
const imageItems = document.querySelectorAll('.image-wrapper > *');
const imageLength = imageItems.length;
const perView = 3;
let totalScroll = 0;
let firstScroll = true;
const delay = 3500;


imageWrapper.style.setProperty('--per-view', perView);
for(let i = 0; i < perView; i++) {
  imageWrapper.insertAdjacentHTML('beforeend', imageItems[i].outerHTML);
}


let autoScroll = setInterval(scrolling, delay);

function scrolling() {
  totalScroll++;
	const firstChild = imageWrapper.firstElementChild;
  imageWrapper.style.left = "-" + (firstChild.offsetWidth).toString() + "px";
  imageWrapper.style.transition = '.3s';
  setTimeout(function() {
      imageWrapper.removeChild(firstChild); 
      imageWrapper.appendChild(firstChild);
      imageWrapper.style.left = 0;
      imageWrapper.style.transition = '0s';
  }, 300);
  firstScroll = false;
}