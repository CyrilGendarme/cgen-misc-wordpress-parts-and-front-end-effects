# Misc WordPress parts and Front-end effects

## Introduction
Welcome to my Git repository showcasing miscellaneous web development projects. Each project focuses on different aspects of web development, including JavaScript, CSS, and PHP. Below is a list of some tiny projects available for review.

## Project List

### 1. glitchEffectOnHover
- **Description**: Create an glitch effect on image hover, and remove it when leaving
- **Technologies Used**: JavaScript, CSS

### 2. infiniteSlidder
- **Description**: A lightweight and SEO-friendly infinite slidder
- **Technologies Used**: JavaScript, CSS, PHP

### 3. miscWpSEO
- **Description**: Misc tiny script to solve some WordPress SEO issues
- **Technologies Used**: JavaScript

### 4. modernGlowingPointer
- **Description**: Replace the pointer with a glowing circle, than change color and size on some HTML elements hover
- **Technologies Used**: JavaScript, CSS

### 5. modernHeader
- **Description**: A UX-friendly header with some custom JavaScript features
- **Technologies Used**: JavaScript, CSS, WP (Divi theme) UI/UX

### 6. shareContentAccrossWpMultisite
- **Description**: Spread products, categories and tags accross multiple subsites (subdirectories) accross a WP multisite installation
- **Technologies Used**: PHP
