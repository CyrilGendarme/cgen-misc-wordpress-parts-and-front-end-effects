  
<?php

function magic_products_mutltilingue() {


    $args = array(
        'limit' => -1, // -1 retrieves all products
    );
    $products = wc_get_products($args);

    $categories = get_terms(array(
        'taxonomy'   => 'product_cat',
        'hide_empty' => false,
    ));

    $tags = get_terms(array(
        'taxonomy'   => 'product_tag',
        'hide_empty' => false,
    ));


    $sites = get_sites();


    foreach ($products as $product) {

        // Retrieve the product data

        $product_id = $product->get_id();
        $product_name = $product->get_name();
        $product_price = $product->get_price();
        $product_regular_price = $product->get_regular_price();
        $product_sale_price = $product->get_sale_price();
        $product_tags = $product->tag_ids;
        $product_categories = wp_get_post_terms($product_id, 'product_cat', array('fields' => 'ids'));
        $product_image_id = get_post_thumbnail_id($product_id);
        $product_image_gallery_ids = $product->get_gallery_image_ids();
        $product_post_name = get_post_field('post_name', $product_id);


        foreach ($sites as $site) {
            // only  client-displayed site
            if ($site->blogname === 'ImageHolder' || $site->blog_id == get_current_blog_id()) {
                continue;
            }

            switch_to_blog($site->blog_id);

            // if product doesn't exist --> create it on the current site
                $args = array(
                    'post_type' => 'product',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                    'title' => $product_name,
                );
                $products_2 = get_posts($args);
            
            if (empty($products_2)) {
                $new_product_id = wp_insert_post(array(
                    'post_title' => $product_name,
                    'post_name' => $product_post_name, // Set the same slug as the previous product
                    'post_type' => 'product',
                    'post_status' => 'publish',
                ));
            }
            else {
                $new_product_id = $products_2[0]->ID;
            }

            // Set product attributes on the current site
            update_post_meta($new_product_id, '_regular_price', $product_regular_price);
            update_post_meta($new_product_id, '_saler_price', $product_sale_price);
            update_post_meta($new_product_id, '_price', $product_price);
            wp_set_object_terms($new_product_id, $product_categories, 'product_cat');
            wp_set_object_terms($new_product_id, $product_tags, 'product_tag');

            update_post_meta( $new_product_id, '_thumbnail_id', $product_image_id );
            update_post_meta( $new_product_id, '_product_image_gallery', implode( ',', $product_image_gallery_ids ) );

            // // Switch back to the main site
            restore_current_blog();
        }
    }



    foreach ($categories as $category) {

        $category_name = $category->name;
        $category_image = get_field('image', 'product_cat_' . $category->term_id);
        $category_instagram = get_field('instagram', 'product_cat_' . $category->term_id);
        $category_site = get_field('site', 'product_cat_' . $category->term_id);
        $category_bio = get_field('Bio', 'product_cat_' . $category->term_id);

        foreach ($sites as $site) {
            // only  client-displayed site
            if ($site->blogname === 'ImageHolder' || $site->blog_id == get_current_blog_id()) {
                continue;
            }

            switch_to_blog($site->blog_id);

            // if category doesn't exist --> create it on the current site
                $category2= get_terms(array(
                    'taxonomy'   => 'product_cat',
                    'name' => $category_name,
                    'hide_empty' => false,
                ));

            if (empty($category2)) {
                $result = wp_create_term($category_name, "product_cat");
                $newId = $result['term_id'];
            }
            else{
                $newId = $category2[0]->term_id;
            }
            update_field('image', $category_image, 'product_cat_' . $newId);
            update_field('instagram', $category_instagram, 'product_cat_' . $newId);
            update_field('site', $category_site, 'product_cat_' . $newId);
            update_field('Bio', $category_bio, 'product_cat_' . $newId);

            // // Switch back to the main site
            restore_current_blog();
        }
    }


    foreach ($tags as $tag) {
        $tag_name = $tag->name;
        $tag_image = get_field('image', 'product_tag_' . $tag->term_id);

        foreach ($sites as $site) {
            // only  client-displayed site
            if ($site->blogname === 'ImageHolder' || $site->blog_id == get_current_blog_id()) {
                continue;
            }

            switch_to_blog($site->blog_id);

            // if category doesn't exist --> create it on the current site
                $tag2 = get_terms(array(
                    'taxonomy'   => 'product_tag',
                    'name' => $tag_name,
                    'hide_empty' => false,
                ));
            
            if (empty($tag2)) {
                $result = wp_create_term($tag_name, "product_tag");
                $newId = $result['term_id'];
            }
            else{
                $newId = $tag2[0]->term_id;
            }
            update_field('image', $tag_image, 'product_tag_' . $newId);
            // Switch back to the main site
            restore_current_blog();
        }
    }




    // second step : delete content that don't exist anymore
    foreach ($sites as $site) {
        if ($site->blogname === 'ImageHolder' || $site->blog_id == get_current_blog_id()) {
            continue;
        }

        switch_to_blog($site->blog_id);
        
        
        $args = array(
            'post_type' => 'product',
            'post_status' => 'publish',
            'posts_per_page' => -1,
        );
        $products_2 = get_posts($args);

        foreach ($products_2 as $product_2) {
            $product_name = $product_2->post_title;

            $product_exists = false;
            foreach ($products as $existing_product) {
                if ($product_name === $existing_product->get_name()) {
                    $product_exists = true;
                    break;
                }
            }

            if (!$product_exists) {
                wp_delete_post($product_2->ID, true);
            }
        }


        $tags_2 = get_terms(array(
            'taxonomy'   => 'product_tag',
            'hide_empty' => false,
        ));

        foreach ($tags_2 as $tag_2) {
            $tag_name = $tag_2->name;

            $tag_exists = false;
            foreach ($tags as $existing_tag) {
                if ($tag_name === $existing_tag->name) {
                    $tag_exists = true;
                    break;
                }
            }

            if (!$tag_exists) {
                wp_delete_term($tag_2->term_id, 'product_tag');
            }
        }

        $categories_2= get_terms(array(
            'taxonomy'   => 'product_cat',
            'hide_empty' => false,
        ));

        foreach ($categories_2 as $category_2) {
            $category_name = $category_2->name;

            $category_exists = false;
            foreach ($categories as $existing_category) {
                if ($category_name === $existing_category->name) {
                    $category_exists = true;
                    break;
                }
            }

            if (!$category_exists) {
                wp_delete_term($category_2->term_id, 'product_category');
            }
        }


        restore_current_blog();
    }

}

?>