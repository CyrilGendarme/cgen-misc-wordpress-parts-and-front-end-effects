
function createGlitchEffect() {

    const imageContainers = document.querySelectorAll('.dmpro_carousel_child .dmpro-carousel-image, .cgen-glitchContainer .et_pb_image, .cgen-glitchContainer .et_shop_image, .cgen-glitchContainer .entry-featured-image-url, .cgen-glitchContainer .cgen-caroussel--img');
  
    imageContainers.forEach(imageContainer => {
      imageContainer.addEventListener("mouseenter", (event) => {
        const image = imageContainer.querySelector("img");
        image.classList.add("glitch1");
  
        const clonedImage = image.cloneNode(true);
        clonedImage.classList.add("glitch2");
        imageContainer.appendChild(clonedImage);
        image.style.opacity = "0.5";
        clonedImage.style.opacity = "0.5";
        clonedImage.style.position = "absolute";
        clonedImage.style.top = "0";
        clonedImage.style.left = "0";
  
        const clonedImage2 = image.cloneNode(true);
        clonedImage2.classList.add("glitch3");
        imageContainer.appendChild(clonedImage2);
        image.style.opacity = "0.5";
        clonedImage2.style.opacity = "0.5";
        clonedImage2.style.position = "absolute";
        clonedImage2.style.top = "0";
        clonedImage2.style.left = "0";
        event.stopImmediatePropagation();
        event.stopPropagation();
      });
  
      imageContainer.addEventListener("mouseleave", (event) => {
        const images = imageContainer.querySelectorAll("img");
        images.forEach((image, index) => {
          if (index === 0) {
            image.classList.remove("glitch1");
            image.style.opacity = "1";
          } else {
            imageContainer.removeChild(image);
          }
        });
        event.stopImmediatePropagation();
        event.stopPropagation();
      });
    });
  }
  
  document.addEventListener('DOMContentLoaded', () => {
    createGlitchEffect();
  
    const container = document.querySelector('.glitchContainer');
    if(container){
        container.addEventListener("mouseenter", () => {
          createGlitchEffect();
        });
      }
  });
      