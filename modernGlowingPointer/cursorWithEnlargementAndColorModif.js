
var cursor;


function enlargeCursorMgmt(isOnLoad) {
  var links = null;
  if (isOnLoad){
	    links = document.querySelectorAll('button');
  }
  else{
	    links = document.querySelectorAll('a, .et_pb_image, button, input, textarea');
  }
	
  // Attach mouseover and mouseout event listeners to links
  links.forEach(link => {
    link.addEventListener('mouseover', e => {
      cursor.classList.add('enlarged');
      e.stopPropagation();
    });
    link.addEventListener('mouseout', e => {
      cursor.classList.remove('enlarged');
      e.stopPropagation();
    });
  });
}
	

function customCursorSetup(){
  cursor = document.createElement('div');
  cursor.classList.add('cursor');
  cursor.style.zIndex = "9999";
  document.body.appendChild(cursor);

  // Track mouse movement and update cursor position
  document.addEventListener('pointermove', e => {
    cursor.style.left = e.clientX + 'px';
    cursor.style.top = e.clientY + 'px';
  }, { capture: true });
}
	
	
function blackAndWhiteBgCursorMgmt(){
	const blackBGs = document.querySelectorAll('.blackBG');
	const notBlackBGs = document.querySelectorAll('input, .selection, textarea, button');
	
  blackBGs.forEach(blackBG => {
    // Attach mouseenter and mouseleave event listeners to blackBG elements
    blackBG.addEventListener('mouseenter', e => {
      cursor.classList.add('cursorOnBlackBG');
      e.stopImmediatePropagation();
    });
    blackBG.addEventListener('mouseleave', e => {
	  const isHoveringOther = Array.from(blackBGs).some(otherBlackBG => otherBlackBG.contains(e.relatedTarget));
      if (!isHoveringOther) {
		  cursor.classList.remove('cursorOnBlackBG');
		  e.stopImmediatePropagation();
	  }
    });
  });
	
  notBlackBGs.forEach(notBlackBG => {
    // Attach mouseenter and mouseleave event listeners to notBlackBG elements
    notBlackBG.addEventListener('mouseenter', e => {
		console.log("debug 1");
      cursor.classList.remove('cursorOnBlackBG');
      e.stopImmediatePropagation();
    });
    notBlackBG.addEventListener('mouseleave', e => {
	  const isHoveringOther = Array.from(blackBGs).some(otherBlackBG => otherBlackBG.contains(e.relatedTarget));
      if (isHoveringOther) {
		  console.log("debug 2");
		  cursor.classList.add('cursorOnBlackBG');
		  e.stopImmediatePropagation();
	  }
    });
  });

}



document.addEventListener('DOMContentLoaded', () => {
    customCursorSetup();
   blackAndWhiteBgCursorMgmt();
   enlargeCursorMgmt(false);
});

document.addEventListener('load', () => {
  enlargeCursorMgmt(true);
});